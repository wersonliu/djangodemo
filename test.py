# -*-coding:utf8-*-
# !/usr/bin/env python
"""
@Time    : 2018/10/26 9:10
@Author  : wersonliu
"""
from sqlalchemy import create_engine
import pandas as pd

db_info = {'user': 'test',
           'password': 'test',
           'host': '35.193.141.39',
           'database': 'lagou'
           }
engine = create_engine('mysql+pymysql://%(user)s:%(password)s@%(host)s/%(database)s?charset=utf8' % db_info,
                       encoding='utf-8')



mpd = pd.read_excel('cxf_lagou_weekly_bak.xlsx')



mpd.to_sql('lagou',engine,index=False)
