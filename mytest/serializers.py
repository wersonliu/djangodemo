# -*-coding:utf8-*-
# !/usr/bin/env python
"""
@Time    : 2018/10/26 9:51
@Author  : wersonliu
"""
from rest_framework import serializers
from mytest.models import Lagou


class LagouSeralizer(serializers.ModelSerializer):
    class Meta:
        model = Lagou
        fields = ('id','title', 'city', 'salary', 'company', 'education')

