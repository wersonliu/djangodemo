# -*-coding:utf8-*-
# !/usr/bin/env python
"""
@Time    : 2018/10/26 9:54
@Author  : wersonliu
"""

from rest_framework.routers import DefaultRouter
from django.urls import path, include
from mytest import views

router = DefaultRouter()
router.register(r'lagou', views.LagouViewSet)
urlpatterns = [
    path('', include(router.urls)),


    path('lagou2/', views.snippet_list),
    path('lagou2/<int:pk>/', views.snippet_detail),

    path('lagou3/', views.lagou_list),
    path('lagou3/<int:pk>/', views.lagou_detail),

    path('lagou4/', views.LagouList.as_view()),
    path('lagou4/<int:pk>/', views.LagouDetail.as_view()),
]
