from django.shortcuts import render

from mytest.serializers import LagouSeralizer
from mytest.models import Lagou

from rest_framework.views import APIView
from rest_framework import viewsets, permissions, renderers
from rest_framework.decorators import detail_route, list_route, action

# 第一种
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

# 第二种
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

# 第二种,基于类
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

#
from rest_framework import mixins
from rest_framework import generics

# Create your views here.


# 使用路由
class LagouViewSet(viewsets.ModelViewSet):
    queryset = Lagou.objects.all()
    serializer_class = LagouSeralizer
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly)

    @action(detail=True, renderer_classes=[renderers.StaticHTMLRenderer])
    def highlight(self, request, *args, **kwargs):
        snippet = self.get_object()
        return Response(snippet.highlighted)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


@csrf_exempt
def snippet_list(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        snippets = Lagou.objects.all()
        serializer = LagouSeralizer(snippets, many=True)
        myobj = {
            'nums': len(snippets),
            'result': serializer.data
        }
        return JsonResponse(myobj, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = LagouSeralizer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


@csrf_exempt
def snippet_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        snippet = Lagou.objects.get(pk=pk)
    except Lagou.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = LagouSeralizer(snippet)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = LagouSeralizer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        snippet.delete()
        return HttpResponse(status=204)


@api_view(['GET', 'POST'])
def lagou_list(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        snippets = Lagou.objects.all()
        serializer = LagouSeralizer(snippets, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = LagouSeralizer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def lagou_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        snippet = Lagou.objects.get(pk=pk)
    except Lagou.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = LagouSeralizer(snippet)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = LagouSeralizer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class LagouList(APIView):
    """
    List all snippets, or create a new snippet.
    """
    def get(self, request, format=None):
        snippets = Lagou.objects.all()
        serializer = LagouSeralizer(snippets, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = LagouSeralizer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LagouDetail(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """

    def get_object(self, pk):
        try:
            return Lagou.objects.get(pk=pk)
        except Lagou.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = LagouSeralizer(snippet)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = LagouSeralizer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


