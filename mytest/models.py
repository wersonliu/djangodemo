# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Lagou(models.Model):
    keyword = models.TextField(blank=True, null=True)
    title = models.TextField(blank=True, null=True,max_length=25)
    city = models.TextField(blank=True, null=True)
    salary = models.TextField(blank=True, null=True)
    company = models.TextField(blank=True, null=True)
    experience = models.TextField(blank=True, null=True)
    education = models.TextField(blank=True, null=True)
    release_date = models.DateTimeField(blank=True, null=True)
    insert_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'lagou'
